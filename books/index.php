
<!DOCTYPE html>
<html>
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<style>
        h1 {-webkit-text-stroke-width: 1px;
            -webkit-text-stroke-color: black;}
        button {display: block;
                margin: 0 auto;
                text-decoration: none;
                padding: 5px 10px;
                background: #da622b;
                border-radius: 10px;
                color: white;
                border-width: 0px;
                
            }
        label {font-family: Palatino Linotype;}
            
        input {border-radius: 15px}
        table, th, td, tr {border: 3px solid white}
        th {-webkit-text-stroke-width: 0.5px;
            -webkit-text-stroke-color: black;}
        tr { background-color: rgba(20, 20, 19, 0.7);}

        form {-webkit-text-stroke-width: 0.1px;
              -webkit-text-stroke-color: black;
            }
        body {background-image: url('1884665.jpg');
              background-size: cover;
              background-attachment: fixed;
              background-blend-mode: darken;
        
            }
        .edit_btn {
              text-decoration: none;
              padding: 2px 5px;
              background: #e4e4eb;
              color: black;
              border-radius: 3px;
            }

        .del_btn {
              text-decoration: none;
              padding: 2px 5px;
              color: black;
              border-radius: 3px;
              background: #e4e4eb;
            }

        .pagination{
                display: block;
                margin: 0 auto;
                text-decoration: none;
                padding: 5px 10px;
                background-color: rgba(20, 20, 19, 0.4);
                border-radius: 10px;
                color: white;
                border-width: 0px;
                font-family: Palatino Linotype;
                font-size: 20px
        }
        .btn btn_dark{
                border: white
        }
</style>

<title>Books</title>
</head>
<body>

<h1 style="font-size:100px;color: rgb(220, 222, 231); font-family: Palatino Linotype; text-align:center; font-style: bold;">Bookstation</h1>

<?php require_once 'db.php'; ?>

<div class="d-flex justify-content-center">
<form action="search.php" method="POST" >
<input style="width:100%;" type="text" name="search" placeholder="Search" ></input>
<button type="submit" name="submit-search" style="font-family: Palatino Linotype; width=60%;">Search</button>
</form><br>

</div>

<div class="row justify-content-center">
<form style="text-align: center; color: white;font-family:'Courier New'; font-size:25px" action="db.php" method="POST">
<div class="form-group">
<label>Name</label><br>
<input type="text"; name="Name" 
       value="<?php echo $name; ?>">
      </div>
<div>
<label>Author</label><br>
<input type="text"; name="Author" 
       value="<?php echo $author; ?>">
      </div>
<div>    
<label>Rating</label><br>
<input type="number"; name="Rating"
       value="<?php echo $rating; ?>">
      </div>
<br>
<br>
<div class="form-group">
      <button type="submit" style="font-family: Palatino Linotype;" name="submit">Submit</button>  
      </div>
    </form>
</div>

<br>
<br>

<div class="row justify-content-center">
<table style="width:80%; color: white;font-family: Palatino Linotype; font-size:25px" class="table">
<thead style="font-size: 30px">
  <tr>
    <th>ID</th>
    <th>Name</th>
    <th>Author</th>
    <th>Rating</th>
    <th colspan="3">Action</th>
    
  </tr>
</thead>
<?php $results = mysqli_query($mysqli, "SELECT * FROM books");
		  while ($row = mysqli_fetch_assoc($result)){ ?>
		<tr>
			<td><?php echo $row['ID']; ?></td>
			<td><?php echo $row['Name']; ?></td>
			<td><?php echo $row['Author']; ?></td>
			<td><?php echo $row['Rating']; ?></td>
      <td>
                        <a href="detail.php?detail=<?php echo $row['ID']; ?>"class="btn btn-outline-light">Detail</a>
      </td>
      <td>
                        <a href="edit.php?edit=<?php echo $row['ID']; ?>" class="btn btn-outline-light" style="color: orange">Edit</a>
			</td>
			<td>
                        <a href="db.php?delete=<?php echo $row['ID']; ?>"class="btn btn-outline-light">Delete</a>
			</td>
            </tr>
          
	<?php } ?>
</div>

<div class="pagination" style="text-align: center; width:25%;">
           <?php 
           if ($current_page > 1 && $total_page > 1){
            echo '<a href="index.php?page='.($current_page-1).'" class="btn btn-outline-light">Prev</a>  ';
        }
        for ($i = 1; $i <= $total_page; $i++){

        if ($i == $current_page){
                echo '<span class="btn btn-light" >'.$i.'</span>  ';
            }
        else{
                echo '<a href="index.php?page='.$i.'" class="btn btn-outline-light">'.$i.'</a>  ';
            }
        }
        if ($current_page < $total_page && $total_page > 1){
            echo '<a href="index.php?page='.($current_page+1).'" class="btn btn-outline-light">Next</a>  ';
        }
           ?>
        </div>