<html>
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<style>
        h1 {-webkit-text-stroke-width: 1px;
            -webkit-text-stroke-color: black;}
        button {display: block;
                margin: 0 auto;
                text-decoration: none;
                padding: 5px 10px;
                background: #da622b;
                border-radius: 10px;
                color: white;
                border-width: 0px;
            }
        label {font-family: Palatino Linotype;
            }
        input {border-radius: 15px}

        form {-webkit-text-stroke-width: 0.1px;
              -webkit-text-stroke-color: black;
            }
        body {background-image: url('1884665.jpg');
              background-size: cover;
              background-attachment: fixed;
              background-blend-mode: darken;
        
            }

</style>

<body>
<h1 style="font-size:100px;color: rgb(220, 222, 231); font-family: Palatino Linotype; text-align:center; font-style: bold;">Bookstation</h1>

		<?php require_once 'db.php';
				$id = $_GET['edit']; 
				$results = mysqli_query($mysqli, "SELECT * FROM books WHERE ID=$id");
					$row = mysqli_fetch_array($results);
                     $name = $row['Name'];
                     $author = $row['Author'];
                     $rating = $row['Rating'];
					 
        ?>	
        <div class="row justify-content-center">
		<form method="post" style="text-align: center; color: white; font-family:Palatino Linotype; font-size:25px">
		<h3 style="font-size:50px;color: rgb(220, 222, 231); font-family: Palatino Linotype; text-align:center;">Edit</h3>
		<div class="form-group">
			<label>Name</label><br>
			<input type="text" name="Name" value="<?php echo $name; ?>">
		</div>
		<div class="form-group">
			<label>Author</label><br>
			<input type="text" name="Author" value="<?php echo $author; ?>">
		</div>
		<div class="form-group">
			<label>Rating</label><br>
			<input type="number" name="Rating" value="<?php echo $rating; ?>">
        </div>
        <br>
		<div class="input-group">
			<button type="submit" name="update" style="text-align: center; font-family: Palatino Linotype;">Update</button>
		
		</div>
    </form>
</div>
	</body>
</html>