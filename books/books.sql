-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2021 at 08:16 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `books`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `ID` int(11) NOT NULL,
  `Name` varchar(8000) NOT NULL,
  `Author` varchar(8000) NOT NULL,
  `Rating` int(11) NOT NULL,
  `Published Year` int(4) NOT NULL,
  `Description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`ID`, `Name`, `Author`, `Rating`, `Published Year`, `Description`) VALUES
(18, 'The Rise and Fall of the Third Reich', 'William L. Shirer', 4, 0, ''),
(19, 'Les Misérables', 'Victor Hugo', 5, 0, ''),
(20, 'Incarceron', 'Catherine Fisher', 4, 0, ''),
(23, 'The Call of the Wild', 'Jack London', 5, 0, ''),
(24, 'Requiem', 'Anna Akhmatova', 4, 0, ''),
(25, 'The Tragedy of X ', 'Frederic Dannay, Manfred Lee', 3, 0, ''),
(26, 'Kafka on the Shore', 'Haruki Murakami', 5, 0, ''),
(27, 'The Wind-up Bird Chronicle', 'Haruki Murakami', 5, 0, ''),
(28, 'Sherlock Holmes', 'Conan Doyle', 4, 0, ''),
(30, 'The Eternal Zero', 'Naoki Hyakuta', 4, 0, ''),
(31, 'b', 'b', 4, 0, ''),
(32, 'a', 'a', 4, 0, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
