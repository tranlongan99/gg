<?php require 'db.php';
  $conn = mysqli_connect ($servername, $username, $password, $dbname);
  if ($conn->connect_error) {
        die("Connection failed:". $conn->connect_error);
  }
  $mysqli = "SELECT ID, Name, Artist, Rating from books";
  $result = $conn->query($mysqli);
  if ($result-> num_rows > 0 ){
        while ($row = $result->fetch_assoc()){
            echo "<tr><td>".$row["ID"]."</td><td>".$row["Name"]."</td><td>".$row["Artist"]."</td><td>".$row["Rating"]."</td><tr>";
        }
      echo "</table>";
  }
  else {
      echo "0 result";
  }
  $conn-> close();
  ?>
</table>


<?php




<?php
	//database connection
	$connection = mysqli_connect('localhost', 'root', '', 'demonuts');
?>
<?php
// Delete data
	if (isset($_GET['del'])) 
	{
		$id = $_GET['del'];
		mysqli_query($connection, "DELETE FROM crud_tbl WHERE id=$id");
	}
?>
<html>
	<head>
		<title>Read All Data</title>
		<link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body>
		<table>
	<thead>
		<tr>
			<th>Id</th>
			<th>Name</th>
			<th>Age</th>
			<th>City</th>
			<th colspan="2">Action</th>
		</tr>
	</thead>
	<?php $results = mysqli_query($connection, "SELECT * FROM crud_tbl");
		  while ($row = mysqli_fetch_array($results)) { ?>
		<tr>
			<td><?php echo $row['id']; ?></td>
			<td><?php echo $row['name']; ?></td>
			<td><?php echo $row['age']; ?></td>
			<td><?php echo $row['city']; ?></td>
			<td>
				<a href="edit.php?edit=<?php echo $row['id']; ?>" class="edit_btn" >Edit</a>
			</td>
			<td>
				<a href="read.php?del=<?php echo $row['id']; ?>" class="del_btn">Delete</a>
			</td>
		</tr>
	<?php } ?>
	<tr>
			<td><a href="index.php"><button type="button" name="back" class="btn">Back</button></a></td>
		</tr>
</table>

	</body>
</html>

<?php
	//database connection
	$connection = mysqli_connect('localhost', 'root', '', 'demonuts');
?>
<?php
// update the data
	if (isset($_POST['update'])) {
	//$id = $_GET['edit'];
	$id = $_GET['edit']; 
	$name = $_POST['name'];
	$age = $_POST['age'];
	$city = $_POST['city'];

	mysqli_query($connection, "UPDATE crud_tbl SET name='$name', age='$age', city='$city' WHERE id=$id");
	echo "Data updated...";
	//$_SESSION['message'] = "Address updated!"; 
	//header('location: index.php');
}
?>
<html>
	<head>
		<title>CRUD Operations</title>
		<link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body>
		<?php 
			//  get the appropriate data of id
				$id = $_GET['edit']; 
				$results = mysqli_query($connection, "SELECT * FROM crud_tbl WHERE id=$id");
					$row = mysqli_fetch_array($results);
					 $name = $row['name'];
					 $age = $row['age'];
					 $city = $row['city'];
		?>	
		<form method="post">
		<h3 align="center"><I>Edit DATA</I></h3>
		<div class="input-group">
			<label>Name</label>
			<input type="text" name="name" value="<?php echo $name; ?>">
		</div>
		<div class="input-group">
			<label>Age</label>
			<input type="text" name="age" value="<?php echo $age; ?>">
		</div>
		<div class="input-group">
			<label>City</label>
			<input type="text" name="city" value="<?php echo $city; ?>">
		</div>
		<div class="input-group">
			<button class="btn" type="submit" name="update" align="center">Edit</button>
			<a href="read.php"><button type="button" name="back" class="btn">Back</button></a>
		</div>
	</form>
	</body>
</html>



<?php include_once '../books/index.php'; ?>
<?php
include_once 'detail.inc.php';
$result = on_get();
?>

<?php start_content('Book detail'); ?>

<?php if ($result === false) : ?>
    <h3 style='color:red;'>Book not found!</h3>
<?php else: $b = $result ?>
    <h1>Book detail</h1>
    <table class="table">
        <tr>
            <th>Title:</th>
            <td><?= $b['title'] ?></td>
        </tr>
        <tr>
            <th>Authors:</th>
            <td><?= $b['authors'] ?></td>
        </tr>
        <tr>
            <th>Publisher:</th>
            <td><?= $b['publisher'] ?></td>
        </tr>
        <tr>
            <th>Year:</th>
            <td><?= $b['year'] ?></td>
        </tr>
        <tr>
            <th>Summary:</th>
            <td><?= $b['description'] ?></td>
        </tr>
    </table>
<?php endif ?>
    <a class="btn btn-outline-primary" href="index.php">Back to list</a>
<?php end_content(); ?>



<!DOCTYPE html>
<html>
<head>
<title>Read Data From Database Using PHP - Demo Preview</title>
<meta content="noindex, nofollow" name="robots">
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="maindiv">
<div class="divA">
<div class="title">
<h2>Read Data Using PHP</h2>
</div>
<div class="divB">
<div class="divD">
<p>Click On Menu</p>
<?php
$connection = mysql_connect("localhost", "root", ""); // Establishing Connection with Server
$db = mysql_select_db("company", $connection); // Selecting Database
//MySQL Query to read data
$query = mysql_query("select * from employee", $connection);
while ($row = mysql_fetch_array($query)) {
echo "<b><a href="readphp.php?id={$row['employee_id']}">{$row['employee_name']}</a></b>";
echo "<br />";
}
?>
</div>
<?php
if (isset($_GET['id'])) {
$id = $_GET['id'];
$query1 = mysql_query("select * from employee where employee_id=$id", $connection);
while ($row1 = mysql_fetch_array($query1)) {
?>
<div class="form">
<h2>---Details---</h2>
<!-- Displaying Data Read From Database -->
<span>Name:</span> <?php echo $row1['employee_name']; ?>
<span>E-mail:</span> <?php echo $row1['employee_email']; ?>
<span>Contact No:</span> <?php echo $row1['employee_contact']; ?>
<span>Address:</span> <?php echo $row1['employee_address']; ?>
</div>
<?php
}
}
?>
<div class="clear"></div>
</div>
<div class="clear"></div>
</div>
</div>
<?php
mysql_close($connection); // Closing Connection with Server
?>
</body>
</html>

?????????????????????????/
