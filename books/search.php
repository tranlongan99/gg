<?php
include 'db.php';
?>

<html>
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<style>
        h1 {-webkit-text-stroke-width: 1px;
            -webkit-text-stroke-color: black;}
        body {background-image: url('1884665.jpg');
              background-size: cover;
              background-attachment: fixed;
              background-blend-mode: darken;}
        table, th, td, tr {border: 3px solid white}
        th {-webkit-text-stroke-width: 0.5px;
            -webkit-text-stroke-color: black;}
        tr { background-color: rgba(20, 20, 19, 0.7);}

        form {-webkit-text-stroke-width: 0.1px;
              -webkit-text-stroke-color: black;}
        .edit_btn {
              text-decoration: none;
              padding: 2px 5px;
              background: #e4e4eb;
              color: black;
              border-radius: 3px;
            }

        .del_btn {
              text-decoration: none;
              padding: 2px 5px;
              color: black;
              border-radius: 3px;
              background: #e4e4eb;
            }
        .resulttt {
            font-size: 30px;
            color: white;
        }
</style>

<body>
<h1 style="font-size:100px;color: rgb(220, 222, 231); font-family: Palatino Linotype; text-align:center; font-style: bold;">Bookstation</h1>
<h2 style="font-size:50px;color: rgb(220, 222, 231); font-family: Palatino Linotype; text-align:center;"> Search </h2>

<div class="row justify-content-center">
<table style="width:80%; color: white;font-family: Palatino Linotype; font-size:25px" class="table">
<thead style="font-size: 30px">
  <tr>
    <th>ID</th>
    <th>Name</th>
    <th>Author</th>
    <th>Rating</th>
    <th colspan="3">Action</th>
    
  </tr>
</thead>
</div>
<div class="container">
<?php
if (isset($_POST['submit-search'])) {
    $search = mysqli_real_escape_string($mysqli, $_POST['search']);
    $sql = "SELECT * FROM books WHERE Name LIKE '%$search%' OR Author LIKE '%$search%' OR Rating LIKE '%$search%' ";
    $result = mysqli_query($mysqli, $sql);
    $queryResult = mysqli_num_rows($result);

    echo "Found ".$queryResult." results!";

    if ($queryResult > 0) {
        while ($row = mysqli_fetch_assoc($result)){
            ?>
		<tr>
			<td><?php echo $row['ID']; ?></td>
			<td><?php echo $row['Name']; ?></td>
			<td><?php echo $row['Author']; ?></td>
			<td><?php echo $row['Rating']; ?></td>
      <td>
                        <a href="detail.php?detail=<?php echo $row['ID']; ?>"class="btn btn-outline-light">Detail</a>
      </td>
      <td>
                        <a href="edit.php?edit=<?php echo $row['ID']; ?>" class="btn btn-outline-light" style="color: orange">Edit</a>
			</td>
			<td>
                        <a href="db.php?delete=<?php echo $row['ID']; ?>"class="btn btn-outline-light">Delete</a>
			</td>
            </tr>
          
	<?php 
        }
    } else {echo "No results";}
}
?>
</div>